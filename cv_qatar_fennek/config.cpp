// Always include the defines common to all wheeled land vehicles at the top of the file.
#include "\vbs2\basic_defines.hpp"
#include "\vbs2\vehicles\vehicles_defines.hpp"

// Always change this to be the path (without drive) to the directory you are working in.
#define __CurrentDir__ \vbs2\customer\vehicles\cv_qatar_fennek

class CfgPatches
{
   // This class can be used by other configurations that may later have dependencies here.
    // The class name is derived from the directory where the configuration is located.
    class vbs2_customer_structures_cv_container
    {
        units[]            = {};
        weapons[]          = {};
        requiredVersion    = 0.10;
        requiredAddons[]   = {};
        modules[] = {};
    };
};

class CfgFactionClasses
{
	class qatar_armed_force
	{
		displayName = "Qatar Armed Forces";
		priority = 1;
		side = TCivilian;
	};
};

class CfgVehicleClasses
{
   class CV_QAT_ARMY_Wheeled
   {
      displayname = "Qatar Armed Force Wheeled";
      country = qatar_armed_force;
   };
};

class CfgVehicles
{
   class vbs_nl_army_fennek_fo_m2_des_x; // Use the closest wheeled land vehicle to your model as your base class from which to inherit.
   //vbs2\vehicles\land\wheeled\kmw_fennek\nl_army_fennek
   class cv_fennek_4x4_vehicle: vbs_nl_army_fennek_fo_m2_des_x // Your wheeled land-vehicle class.
   {
      side               = TCivilian;
      displayName        = "Fennek Covisart";
      vehicleClass       = CV_QAT_ARMY_Wheeled;
      faction            = qatar_armed_force;
      
      hiddenSelectionstextures[] = 
      {
         __CurrentDir__\qatar\texture.paa,
         "\vbs2\vehicles\Land\Wheeled\kmw_fennek\nl_army_fennek\data\nl_army_fennek_ext_02_des_ca",
         "\vbs2\vehicles\Land\Wheeled\kmw_fennek\nl_army_fennek\data\nl_army_slat_armour_ext_01_des_co"
      };
      class EventHandlers
      {
         //init = "_this execVM '\vbs2\customer\vehicles\cv_qatar_fennek\load.sqf'";
      };
      class Library
      {
         libTextDesc = "Fennek Covisart";
      };    
   };
};