// Always include the defines common to all wheeled land vehicles at the top of the file.
#include "\vbs2\basic_defines.hpp"
#include "\vbs2\vehicles\vehicles_defines.hpp"

// Always change this to be the path (without drive) to the directory you are working in.
#define __CurrentDir__ \vbs2\customer\vehicles\cv_qatar_rafale

class CfgPatches
{
    // This class can be used by other configurations that may later have dependencies here.
    // The class name is derived from the directory where the configuration is located.
    class cv_qatar_rafale
    {
        units[]            = {};
        weapons[]          = {};
        requiredVersion    = 0.10;
        requiredAddons[]   = {};
        modules[]          = {};
    };
};
class CfgVehicles
{
	class vbs_fr_af_rafale_gry_gbu12_x;
	class cv_qatar_rafale : vbs_fr_af_rafale_gry_gbu12_x
	{
		displayName = "Dassault Rafale";
		hiddenSelectionsTextures[] = {__CurrentDir__\Texture.paa};
		side               = TCivilian;
		vehicleClass       = CV_QAT_ARMY_Wheeled;
      	faction            = qatar_armed_force;
	}
}