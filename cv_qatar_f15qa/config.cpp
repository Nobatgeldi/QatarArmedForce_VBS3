// Always include the defines common to all wheeled land vehicles at the top of the file.
#include "\vbs2\basic_defines.hpp"
#include "\vbs2\vehicles\vehicles_defines.hpp"

// Always change this to be the path (without drive) to the directory you are working in.
#define __CurrentDir__ \vbs2\customer\vehicles\cv_qatar_f15qa

class CfgPatches
{
    // This class can be used by other configurations that may later have dependencies here.
    // The class name is derived from the directory where the configuration is located.
    class cv_qatar_f15qa
    {
        units[]            = {};
        weapons[]          = {};
        requiredVersion    = 0.10;
        requiredAddons[]   = {};
        modules[] 		   = {};
    };
};
class CfgVehicles
{
	class vbs_us_af_f15e_litening_gry_gbu12_x;
	class cv_qatar_f15qa : vbs_us_af_f15e_litening_gry_gbu12_x
	{
		displayName = "F-15QA";
		hiddenSelectionsTextures[] = {__CurrentDir__\texture.paa};
		side               = TCivilian;
		vehicleClass       = CV_QAT_ARMY_Wheeled;
      	faction            = qatar_armed_force;
	}
}
