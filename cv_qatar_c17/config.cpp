// Always include the defines common to all wheeled land vehicles at the top of the file.
#include "\vbs2\basic_defines.hpp"
#include "\vbs2\vehicles\vehicles_defines.hpp"

// Always change this to be the path (without drive) to the directory you are working in.
#define __CurrentDir__ \vbs2\customer\vehicles\cv_qatar_c17

class CfgPatches
{
    // This class can be used by other configurations that may later have dependencies here.
    // The class name is derived from the directory where the configuration is located.
    class cv_qatar_c17
    {
        units[]            = {};
        weapons[]          = {};
        requiredVersion    = 0.10;
        requiredAddons[]   = {};
        modules[] 		   = {};
    };
};
class CfgVehicles
{
	//configFile >> "CfgVehicles">>"vbs_us_af_c17_gry_x"
	class vbs_us_af_c17_gry_x;
	class cv_qatar_c17_gry : vbs_us_af_c17_gry_x
	{
		displayName = "C-17A Globemaster III";
		side               = TCivilian;
		vehicleClass       = CV_QAT_ARMY_Wheeled;
      	faction            = qatar_armed_force;
		
		hiddenSelectionsTextures[] = 
		{
			__CurrentDir__\texture.paa,
			"\vbs2\vehicles\Air\Planes\boeing_c17\usaf_c17\data\usaf_c17_wings_co"
		};
		//__CurrentDir__\texture_fus.paa
	}
	//configFile >> "CfgVehicles" >> "vbs_us_af_c17_gry_palletedseats_x"
	class vbs_us_af_c17_gry_palletedseats_x;
	class cv_qatar_c17_palletedseats : vbs_us_af_c17_gry_palletedseats_x
	{
		displayName = "C-17A Globemaster III - Cargo Seats";
		hiddenSelectionsTextures[] = 
		{
			__CurrentDir__\texture.paa,
			"\vbs2\vehicles\Air\Planes\boeing_c17\usaf_c17\data\usaf_c17_wings_co"
		};
		//__CurrentDir__\texture_fus.paa
		side               = TCivilian;
		vehicleClass       = CV_QAT_ARMY_Wheeled;
      	faction            = qatar_armed_force;
	}
}
